sap.ui.define([
    "iba/gomel/by/myshop/formatter/priceFormatter"
], function (priceFormatter) {
    "use strict";

    QUnit.module("Price formatter", {
        beforeEach: function () {
            this.oPriceFormatter = priceFormatter;

            this.aProductList1 = [
                {"IsBuyNow" : true, Count : 2, Detail : {Price : 200}},
                {"IsBuyNow" : true, Count : 1, Detail : {Price : 150}},
                {"IsBuyNow" : true, Count : 5, Detail : {Price : 50}}
            ];
            this.aProductList2 = [
                {"IsBuyNow" : true, Count : 5, Detail : {Price : 375}},
                {"IsBuyNow" : false, Count : 15, Detail : {Price : 412}},
                {"IsBuyNow" : false, Count : 2, Detail : {Price : 560}}
            ];
            this.aProductList3 = [
                {"IsBuyNow" : false, Count : 1, Detail : {Price : 200}}
            ];
            this.aProductList4 = []

            this.oCurrency1 = {Ratio : 1.5};
            this.oCurrency2 = {Ratio : 1.75};
            this.oCurrency3 = {Ratio : 0.8};
            this.oCurrency4 = {Ratio : 2.11};
        },
        afterEach: function () {}
    });

    QUnit.test("Counting total price", function (assert) {
        assert.strictEqual(this.oPriceFormatter.totalPrice(this.aProductList1, this.oCurrency1), 1200, "Input: 2x200, 1x150, 5x50; Ratio - 1.5. Output: 1200");
        assert.strictEqual(this.oPriceFormatter.totalPrice(this.aProductList2, this.oCurrency2), 3281.25, "Input: 5x375; Ratio - 1.75. Output: 3281.25");
        assert.strictEqual(this.oPriceFormatter.totalPrice(this.aProductList3, this.oCurrency3), 0, "Input: 0; Ratio - 0.8. Output: 0");    
        assert.strictEqual(this.oPriceFormatter.totalPrice(this.aProductList4, this.oCurrency4), 0, "Input: 0; Ratio - 2.11. Output: 0");
        
        assert.strictEqual(this.oPriceFormatter.totalPrice(this.aProductList1, this.oCurrency2), 1400, "Input: 2x200, 1x150, 5x50; Ratio - 1.75. Output: 1400");
        assert.strictEqual(this.oPriceFormatter.totalPrice(this.aProductList1, this.oCurrency3), 640, "Input: 2x200, 1x150, 5x50; Ratio - 0.8. Output: 640");
        assert.strictEqual(this.oPriceFormatter.totalPrice(this.aProductList1, this.oCurrency4), 1688, "Input: 2x200, 1x150, 5x50; Ratio - 2.11. Output: 1688");

        assert.strictEqual(this.oPriceFormatter.totalPrice(this.aProductList2, this.oCurrency1), 2812.5, "Input: 5x375; Ratio - 1.75. Output: 2812.5");
        assert.strictEqual(this.oPriceFormatter.totalPrice(this.aProductList2, this.oCurrency3), 1500, "Input: 5x375; Ratio - 0.8. Output: 1500");
        assert.strictEqual(this.oPriceFormatter.totalPrice(this.aProductList2, this.oCurrency4), 3956.2499999999995, "Input: 5x375; Ratio - 2.11. Output: 3956.2499999999995");
    })
});