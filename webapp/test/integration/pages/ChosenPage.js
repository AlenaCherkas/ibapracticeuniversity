sap.ui.define([
    "sap/ui/test/Opa5",
    "sap/ui/test/actions/Press",
    "sap/ui/test/actions/EnterText",
    "sap/ui/test/matchers/AggregationLengthEquals",
    "sap/ui/test/matchers/Properties",
    "sap/ui/test/matchers/BindingPath"
], function (Opa5, Press, EnterText, AggregationLengthEquals, Properties, BindingPath) {
    "use strict";

    var sViewName = "iba.gomel.by.myshop.view.Chosen";

    Opa5.createPageObjects({
        onTheChosenPage: {
            actions: {
                iRemoveChosenProduct: function (iPosition) {
                    return this.waitFor({
                        controlType: "sap.m.Button",
                        matchers: [
                                new Properties({
                                    icon: "sap-icon://decline"
                                }),
                                new BindingPath({
                                    path: "/" + iPosition,
                                    modelName: "chosens"
                                })
                        ],                       
                        viewName: sViewName,
                        actions: new Press(),
                        errorMessage: "Did not find the 'remove' button on grid item."
                    });
                },
                iPressAddToCardFromChosenPageButton: function (iPosition) {
                    return this.waitFor({
                        controlType: "sap.m.Button",
                        matchers: [
                                new Properties({
                                    icon: "sap-icon://cart-5"
                                }),
                                new BindingPath({
                                    path: "/" + iPosition,
                                    modelName: "chosens"
                                })
                        ],                       
                        viewName: sViewName,
                        actions: new Press(),
                        errorMessage: "Did not find the 'add to card' button on grid item."
                    });
                }                 
            },

            assertions: {
                theChosenGridHaveSomeEntries: function (iCount) {
                    return this.waitFor({
                        controlType: "sap.f.GridList",
                        matchers:  new AggregationLengthEquals({
                            name: "items",
                            length: iCount
                        }),
                        success: function () {
                            Opa5.assert.ok(true, iCount + " items have been found");
                        },
                        errorMessage: iCount + " items have not been found"
                    });
                },                                          
            }
        }
    });
});