sap.ui.define([
    "sap/ui/test/Opa5",
    "sap/ui/test/actions/Press",
    "sap/ui/test/actions/EnterText",
    "sap/ui/test/matchers/AggregationLengthEquals",
    "sap/ui/test/matchers/Properties"
], function (Opa5, Press, EnterText, AggregationLengthEquals, Properties) {
    "use strict";

    var sViewName = "iba.gomel.by.myshop.view.UserDetails";

    Opa5.createPageObjects({
        onTheUserPage: {
            actions: {
                iMoveToContactOption: function () {
                    return this.waitFor({
                        controlType: "sap.m.StandardListItem",
                        matchers: new Properties({
                            title: "Contact"
                        }),
                        actions: new Press(),
                        errorMessage: "Did not find the contact option page"
                    });
                },
                iMoveToPrivateOption: function () {
                    return this.waitFor({
                        controlType: "sap.m.StandardListItem",
                        matchers: new Properties({
                            title: "Private"
                        }),
                        actions: new Press(),
                        errorMessage: "Did not find the orivate option page"
                    });
                },
                iMoveToDeliveryOption: function () {
                    return this.waitFor({
                        controlType: "sap.m.StandardListItem",
                        matchers: new Properties({
                            title: "Delivery"
                        }),
                        actions: new Press(),
                        errorMessage: "Did not find the delivery option page"
                    });
                },
                iPressEditButton: function () {
                    return this.waitFor({
                        id: "edit",
                        viewName: sViewName,
                        actions: new Press(),
                        errorMessage: "Did not find the 'Edit' button on user info page."
                    });
                },
                iPressCancelButton: function () {
                    return this.waitFor({
                        id: "reject",
                        viewName: sViewName,
                        actions: new Press(),
                        errorMessage: "Did not find the 'Cancel' button on user info page."
                    });
                },
            },

            assertions: {
                iShouldSeeUserPage: function () {
                    return this.waitFor({
                        controlType: "sap.m.SplitApp",
                        success: function () {
                            Opa5.assert.ok(true, "User page have been found");
                        },
                        errorMessage: "Did not find the user page"
                    });
                },
                IamOnContactPageOption: function () {
                    return this.waitFor({
                        controlType: "sap.m.Page",
                        matchers: new Properties({
                            title: "Contact information"
                        }),
                        success: function () {
                            Opa5.assert.ok(true, "Navigated to contact page option");
                        },
                        errorMessage: "Not navigated to contact page option"
                    });
                },
                IamOnDeliveryPageOption: function () {
                    return this.waitFor({
                        controlType: "sap.m.Page",
                        matchers: new Properties({
                            title: "Delivery information"
                        }),
                        success: function () {
                            Opa5.assert.ok(true, "Navigated to delivery page option");
                        },
                        errorMessage: "Not navigated to delivery page option"
                    });
                },
                IamOnPrivatePageOption: function () {
                    return this.waitFor({
                        controlType: "sap.m.Page",
                        matchers: new Properties({
                            title: "Private information"
                        }),
                        success: function () {
                            Opa5.assert.ok(true, "Navigated to private page option");
                        },
                        errorMessage: "Not navigated to private page option"
                    });
                },
                editModeIsActive: function () {
                    return this.waitFor({
                        controlType: "sap.m.Button",
                        matchers: new Properties({
                            text: "Save"
                        }),
                        success: function () {
                            Opa5.assert.ok(true, "Edit mode is active");
                        },
                        errorMessage: "Edit mode is not active"
                    });
                },                
                editModeIsNotActive: function () {
                    return this.waitFor({
                        controlType: "sap.m.Button",
                        matchers: new Properties({
                            text: "Edit"
                        }),
                        success: function () {
                            Opa5.assert.ok(true, "Edit mode is not active");
                        },
                        errorMessage: "Edit mode is active"
                    });
                }                          
            }
        }
    });
});