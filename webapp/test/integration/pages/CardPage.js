sap.ui.define([
    "sap/ui/test/Opa5",
    "sap/ui/test/actions/Press",
    "sap/ui/test/actions/EnterText",
    "sap/ui/test/matchers/AggregationLengthEquals",
    "sap/ui/test/matchers/Properties",
    "sap/ui/test/matchers/BindingPath"
], function (Opa5, Press, EnterText, AggregationLengthEquals, Properties, BindingPath) {
    "use strict";

    var sViewName = "iba.gomel.by.myshop.view.Card";

    Opa5.createPageObjects({
        onTheCardPage: {
            actions: {
                iPressCheckBoxBuyNow: function (iPosition) {
                    return this.waitFor({
                        controlType: "sap.m.CheckBox",
                        matchers: [
                            new BindingPath({
                                path: "/" + iPosition,
                                modelName: "goods"
                            })
                        ],                       
                        viewName: sViewName,
                        actions: new Press(),
                        errorMessage: "Did not find the 'check box' on grid item."
                    });
                },
                iPressIncreaseOnStepInput: function (iPosition) {
                    return this.waitFor({
                        controlType: "sap.m.StepInput",
                        matchers: [
                            new BindingPath({
                                path: "/" + iPosition,
                                modelName: "goods"
                            })
                        ],                       
                        viewName: sViewName,
                        actions: function (oStepInput) {
                            var iStep = oStepInput.getStep();
                            var iValue = oStepInput.getValue()
                            oStepInput.setValue(iValue + iStep);
                        },
                        errorMessage: "Did not find the 'step input' on grid item."
                    });
                },
                iPressCleanUpButton: function () {
                    return this.waitFor({
                        id: "cleanUp",
                        viewName: sViewName,
                        actions: new Press(),
                        errorMessage: "Did not find the 'Clean Up' button on card page."
                    });
                },
                iPressCheckoutButton: function () {
                    return this.waitFor({
                        id: "checkOut",
                        viewName: sViewName,
                        actions: new Press(),
                        success: function () {
                            this.waitFor({
                                controlType: "sap.m.Button",
                                matchers: new Properties({
                                    text: "OK"
                                }),
                                searchOpenDialogs: true,
                                actions: new Press(),
                                errorMessage: "Didn't find OK button on confirm dialog"
                            })
                        },
                        errorMessage: "Did not find the 'Check out' button on card page."
                    });
                },
                iPressShowMoreInfo: function (iPosition) {
                    return this.waitFor({
                        controlType: "sap.m.Button",
                        matchers: [
                            new Properties({
                                icon: "sap-icon://hint"
                            }),
                            new BindingPath({
                                path: "/" + iPosition,
                                modelName: "goods"
                            })
                        ],                       
                        viewName: sViewName,
                        actions: new Press(),
                        errorMessage: "Did not find the 'show more info' button on grid item."
                    });
                },
                iRemoveProductsOfOneType: function (iPosition) {
                    return this.waitFor({
                        controlType: "sap.m.Button",
                        matchers: [
                                new Properties({
                                    icon: "sap-icon://decline"
                                }),
                                new BindingPath({
                                    path: "/" + iPosition,
                                    modelName: "goods"
                                })
                        ],                       
                        viewName: sViewName,
                        actions: new Press(),
                        errorMessage: "Did not find the 'remove' button on grid item."
                    });
                }                
            },

            assertions: {
                theCardHaveSomeEntries: function (iCount) {
                    return this.waitFor({
                        controlType: "sap.f.GridList",
                        matchers:  new AggregationLengthEquals({
                            name: "items",
                            length: iCount
                        }),
                        success: function () {
                            Opa5.assert.ok(true, iCount + " items have been found");
                        },
                        errorMessage: iCount + " items have not been found"
                    });
                },
                theCardHaveNoEntries: function() {
                    return this.waitFor({
                        controlType: "sap.f.GridList",
                        matchers:  new AggregationLengthEquals({
                            name: "items",
                            length: 0
                        }),
                        success: function () {
                            Opa5.assert.ok(true, "There is no any item");
                        },
                        errorMessage: "There are some items"
                    });
                },
                iShouldSeeAdditionalInfo: function () {
                    return this.waitFor({
                        controlType: "sap.m.QuickView",
                        success: function () {
                            Opa5.assert.ok(true, "The 'Additinal info' popup is open");
                        },
                        errorMessage: "Did not find the popup control"
                    });
                },
                iShouldSeeIncreasedProductCount: function (iPosition, iCount) {
                    return this.waitFor({
                        controlType: "sap.m.StepInput",
                        matchers: [
                            new Properties({
                                value: iCount
                            }),
                            new BindingPath({
                                path: "/" + iPosition,
                                modelName: "goods"
                            })
                        ],                       
                        viewName: sViewName,
                        success: function () {
                            Opa5.assert.ok(true, "Increase action was successfull");
                        },
                        errorMessage: "Increase action was not successfull"
                    });
                },                                             
            }
        }
    });
});