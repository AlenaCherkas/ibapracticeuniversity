sap.ui.define([
    "sap/ui/test/Opa5",
    "sap/ui/test/actions/Press",
    "sap/ui/test/actions/EnterText",
    "sap/ui/test/matchers/AggregationLengthEquals",
    "sap/ui/test/matchers/Properties",
    "sap/ui/test/matchers/BindingPath"
], function (Opa5, Press, EnterText, AggregationLengthEquals, Properties, BindingPath) {
    "use strict";

    var sViewName = "iba.gomel.by.myshop.view.DetailsList";
    var sMenuViewName = "iba.gomel.by.myshop.view.Menu";
    var sDetailViewName = "iba.gomel.by.myshop.view.Detail";

    Opa5.createPageObjects({
        onTheAppPage: {
            actions: {
                iPressItem: function (iPosition) {
                    return this.waitFor({
                        controlType: "sap.m.ObjectListItem",
                        matchers: [
                            new BindingPath({
                                path: "/Details(" + iPosition +")",
                                modelName: "oDataModel"
                            })
                        ],                       
                        viewName: sViewName,
                        actions: new Press(),
                        errorMessage: "Did not find item."
                    });
                },
                iAddToChosen: function () {
                    return this.waitFor({
                        id: "addToChosen",
                        viewName: sDetailViewName,
                        actions: new Press(),
                        errorMessage: "Did not find the 'add to chosen' button on Detail page."
                    });
                },
                iNavigateToChosenPage: function () {
                    return this.waitFor({
                        id: "toChosen",
                        viewName: sMenuViewName,
                        actions: new Press(),
                        errorMessage: "Did not find the 'Chosen Page' button on Menu."
                    });
                },
                iNavigateToUserPage: function () {
                    return this.waitFor({
                        id: "toUserpage",
                        viewName: sMenuViewName,
                        actions: new Press(),
                        errorMessage: "Did not find the 'User Page' button on Menu."
                    });
                },
                iNavigateToCardPage: function () {
                    return this.waitFor({
                        id: "toCardpage",
                        viewName: sMenuViewName,
                        actions: new Press(),
                        errorMessage: "Did not find the 'Card Page' button on Menu."
                    });
                },
                iPressSortButton: function () {
                    return this.waitFor({
                        id: "sortButton",
                        viewName: sViewName,
                        actions: new Press(),
                        errorMessage: "Did not find the 'Sort' button on Product list page."
                    });
                },
                iPressGroupButton: function () {
                    return this.waitFor({
                        id: "groupButton",
                        viewName: sViewName,
                        actions: new Press(),
                        errorMessage: "Did not find the 'Group' button on Product list page."
                    });
                },
                iPressFilterButton: function () {
                    return this.waitFor({
                        id: "filterButton",
                        viewName: sViewName,
                        actions: new Press(),
                        errorMessage: "Did not find the 'Filter' button on Product list page."
                    });
                },
                iSearchForEnterText: function (sText) {
                    return this.waitFor({
                        controlType: "sap.m.SearchField",
                        viewName: sViewName,
                        actions: new EnterText({
                            text: sText
                        }),
                        errorMessage: "Did not find the search field."
                    });
                },
                iChooseGrouping: function (sGroupBy) {
                    return this.waitFor({
                        id: "groupButton",
                        viewName: sViewName,
                        actions: new Press(),
                        success: function () {
                            this.waitFor({
                                controlType: "sap.m.StandardListItem",
                                matchers: new Properties({
                                    title: sGroupBy
                                }),
                                searchOpenDialogs: true,
                                actions: new Press(),
                                success: function () {
                                    this.waitFor({
                                        controlType: "sap.m.Button",
                                        matchers: new Properties({
                                            text: "OK"
                                        }),
                                        searchOpenDialogs: true,
                                        actions: new Press(),
                                        errorMessage: "Didn't find OK button"
                                    })
                                },
                                errorMessage: "Did not find the " + sGroupBy + " group option in dialog"                             
                            })
                        },
                        errorMessage: "Did not find the 'Group' button on Product list page."                        
                    })
                },
                iFilterTheListOn: function (sFilterBy) {
                    return this.waitFor({
                        id: "filterButton",
                        viewName: sViewName,
                        actions: new Press(),
                        success: function () {
                            this.waitFor({
                                controlType: "sap.m.StandardListItem",
                                matchers: new Properties({
                                    title: "Type"
                                }),
                                actions: new Press(),
                                success: function () {
                                    this.waitFor({
                                        controlType: "sap.m.StandardListItem",
                                        matchers: new Properties({
                                            title: sFilterBy
                                        }),
                                        actions: new Press(),
                                        success: function () {
                                            this.waitFor({
                                                controlType: "sap.m.Button",
                                                matchers: new Properties({
                                                    text: "OK"
                                                }),
                                                searchOpenDialogs: true,
                                                actions: new Press(),
                                                errorMessage: "Didn't find OK button"
                                            })
                                        },
                                        errorMessage: "Error"                                        
                                    })
                                },
                                errorMessage: "Did not find the Type filter option in dialog"
                            })
                        },
                        errorMessage: "Did not find the 'Filter' button on Product list page."
                    })
                },
                iSortTheListBy: function() {
                    return this.waitFor({
                        id: "sortButton",
                        viewName: sViewName,
                        actions: new Press(),
                        success: function () {
                            this.waitFor({
                                controlType: "sap.m.StandardListItem",
                                matchers: new Properties({
                                    title: "Name"
                                }),
                                actions: new Press(),
                                success: function () {
                                    this.waitFor({
                                        controlType: "sap.m.Button",
                                        matchers: new Properties({
                                            text: "OK"
                                        }),
                                        searchOpenDialogs: true,
                                        actions: new Press(),
                                        errorMessage: "Didn't find OK button"
                                    })        
                                },
                                errorMessage: "Did not find the 'Sort' option by name"                        
                            })  
                        },
                        errorMessage: "Did not find the 'Sort' button on Product list page."                        
                    })                    
                }
            },

            assertions: {
                iShouldSeeSortDialog: function () {
                    return this.waitFor({
                        controlType: "sap.m.Dialog",
                        success: function () {
                            Opa5.assert.ok(true, "The 'Sort' dialog is open");
                        },
                        errorMessage: "Did not find the dialog control"
                    });
                },
                iShouldSeeGroupDialog: function () {
                    return this.waitFor({
                        controlType: "sap.m.Dialog",
                        success: function () {
                            Opa5.assert.ok(true, "The 'Group' dialog is open");
                        },
                        errorMessage: "Did not find the dialog control"
                    });
                },
                iShouldSeeFilterDialog: function () {
                    return this.waitFor({
                        controlType: "sap.m.Dialog",
                        success: function () {
                            Opa5.assert.ok(true, "The 'Filter' dialog is open");
                        },
                        errorMessage: "Did not find the dialog control"
                    });
                },

                theListShouldHaveSomeEntries: function (iCount) {
                    return this.waitFor({
                        controlType: "sap.m.List",
                        matchers:  new AggregationLengthEquals({
                            name: "items",
                            length: iCount
                        }),
                        success: function () {
                            Opa5.assert.ok(true, iCount + " items have been found");
                        },
                        errorMessage: iCount + " items have not been found"
                    });
                },

                theListShouldContainAGroupHeader: function () {
                    return this.waitFor({
                        controlType: "sap.m.GroupHeaderListItem",
                        success: function () {
                            Opa5.assert.ok(true, "Product List is grouped");
                        },
                        errorMessage: "Product List is not grouped"
                    })
                },

                theListShouldBeFilteredByType: function (sTypeName) {
                    return this.waitFor({
                        controlType: "sap.m.List",
                        matchers: [
                            new AggregationLengthEquals({
                                name: "items",
                                length: 2
                            }),
                            function fnCheckFilters (oList) {
                                var bIsTrue = true;
                                var iLength = oList.getItems().length;
                                for (var i = 0; i < iLength; i++) {
                                    var oItem = oList.getItems()[i];
                                    if (!oItem.getBindingContext("oDataModel")) {
                                        bIsTrue = false;                                
                                    } else {
                                        var sType = oItem.getBindingContext("oDataModel").getProperty("Type");
                                        if (!sType || sType != sTypeName) {
                                            bIsTrue = false;
                                        }    
                                    }                      
                                }
                                return bIsTrue;
                            }
                        ],
                        success: function () {
                            Opa5.assert.ok(true, "List have been filtered by type");
                        },
                        errorMessage: "List have not been filtered by type"                                                
                    });                
                },

                theListShouldBeSortedByName: function () {
                    return this.waitFor({
                        controlType: "sap.m.List",
                        matchers: [
                            new AggregationLengthEquals({
                                name: "items",
                                length: 6
                            }),
                            function fnCheckSort (oList) {
                                var bIsTrue = true;
                                return bIsTrue;
                            }
                        ],
                        success: function () {
                            Opa5.assert.ok(true, "List have been sorted by name");
                        },
                        errorMessage: "List have not been sorted by name"                                                
                    });                
                }
            }
        }
    });
});