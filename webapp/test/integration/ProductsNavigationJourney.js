sap.ui.define([
    "sap/ui/test/opaQunit",
    "./pages/ProductPage",
    "./pages/UserPage"
], function () {
    "use strict";

    var oConfig = {
        componentConfig: {
            name: "iba.gomel.by.myshop"
        }
    }

    QUnit.module("Main panel button pressing");

    opaTest("Should open the 'Sort' dialog", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iPressSortButton();
        Then.onTheAppPage.iShouldSeeSortDialog().and.iTeardownMyApp();

    });

    opaTest("Should open the 'Group 'dialog", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iPressGroupButton();
        Then.onTheAppPage.iShouldSeeGroupDialog().and.iTeardownMyApp();
    });

    opaTest("Should open the 'Filter 'dialog", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iPressFilterButton();
        Then.onTheAppPage.iShouldSeeFilterDialog().and.iTeardownMyApp();
    });

    QUnit.module("Searching for products");
    opaTest("Should find 2 products by name, year or type", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iSearchForEnterText("Ga");
        Then.onTheAppPage.theListShouldHaveSomeEntries(2).and.iTeardownMyApp();
    });

    opaTest("Should find 0 products by name, year or type", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iSearchForEnterText("ASg4ggsdfv");
        Then.onTheAppPage.theListShouldHaveSomeEntries(0).and.iTeardownMyApp();
    });

    opaTest("Should find all 6 products by name, year or type", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iSearchForEnterText("");
        Then.onTheAppPage.theListShouldHaveSomeEntries(6).and.iTeardownMyApp();
    });

    QUnit.module("Work with dialogs");

    opaTest("Should group items by key", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iChooseGrouping("Year");
        Then.onTheAppPage.theListShouldContainAGroupHeader().and.iTeardownMyApp();
    });

    opaTest("Should filter items by type", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iFilterTheListOn("HDD");
        Then.onTheAppPage.theListShouldBeFilteredByType("HDD").and.iTeardownMyApp();
    });

    opaTest("Should sort items by name(asce)", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iSortTheListBy();
        Then.onTheAppPage.theListShouldBeSortedByName().and.iTeardownMyApp();
    });

    QUnit.module("Navigation to other pages");

    opaTest("Should navigate to user page", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iNavigateToUserPage();
        Then.onTheUserPage.iShouldSeeUserPage().and.iTeardownMyApp();
    });
});