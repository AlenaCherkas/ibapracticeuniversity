sap.ui.define([
    "sap/ui/test/opaQunit",
    "./pages/ProductPage",
    "./pages/UserPage",
    "./pages/CardPage",
    "./pages/ChosenPAge"
], function () {
    "use strict";

    var oConfig = {
        componentConfig: {
            name: "iba.gomel.by.myshop"
        }
    }

    QUnit.module("Interaction with chosen products");

    opaTest("Should remove chosen product", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iNavigateToChosenPage();
        When.onTheChosenPage.iRemoveChosenProduct(1);

        Then.onTheChosenPage.theChosenGridHaveSomeEntries(1).and.iTeardownMyApp();

    });

    opaTest("Should add product to card from chosen page", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iNavigateToChosenPage();
        When.onTheChosenPage.iPressAddToCardFromChosenPageButton(0);
        When.onTheAppPage.iNavigateToCardPage();

        Then.onTheCardPage.theCardHaveSomeEntries(3).and.iTeardownMyApp();
    });

    opaTest("Should add product to card from chosen page", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iPressItem(3);
        When.onTheAppPage.iAddToChosen();
        When.onTheAppPage.iNavigateToChosenPage();

        Then.onTheChosenPage.theChosenGridHaveSomeEntries(2).and.iTeardownMyApp();

    });
});