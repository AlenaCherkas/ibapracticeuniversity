sap.ui.define([
    "sap/ui/test/opaQunit",
    "./pages/ProductPage",
    "./pages/UserPage",
    "./pages/CardPage"
], function () {
    "use strict";

    var oConfig = {
        componentConfig: {
            name: "iba.gomel.by.myshop"
        }
    }

    QUnit.module("Main menu options");

    opaTest("Should navigate clean up the card", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iNavigateToCardPage();
        When.onTheCardPage.iPressCleanUpButton();

        Then.onTheCardPage.theCardHaveSomeEntries(0).and.iTeardownMyApp();
    });

    opaTest("Should confirm checkout", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iNavigateToCardPage();
        When.onTheCardPage.iPressCheckoutButton();
        
        Then.onTheCardPage.theCardHaveNoEntries().and.iTeardownMyApp();

    });

    QUnit.module("Actions on grid item");
    
    opaTest("Should show product info", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iNavigateToCardPage();
        When.onTheCardPage.iPressShowMoreInfo(1);

        Then.onTheCardPage.iShouldSeeAdditionalInfo().and.iTeardownMyApp()

    });
    
    opaTest("Should remove product from card", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iNavigateToCardPage();
        When.onTheCardPage.iRemoveProductsOfOneType(1);

        Then.onTheCardPage.theCardHaveSomeEntries(1).and.iTeardownMyApp()
    });

    opaTest("Add one more product", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iNavigateToCardPage();
        When.onTheCardPage.iPressIncreaseOnStepInput(1);
        
        Then.onTheCardPage.iShouldSeeIncreasedProductCount(1,5).and.iTeardownMyApp();
    });

    opaTest("Confirm order when some product is not mapped as 'buy now'", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iNavigateToCardPage();
        When.onTheCardPage.iPressCheckBoxBuyNow(1);
        When.onTheCardPage.iPressCheckoutButton();

        Then.onTheCardPage.theCardHaveSomeEntries(1).and.iTeardownMyApp();      
    });
});