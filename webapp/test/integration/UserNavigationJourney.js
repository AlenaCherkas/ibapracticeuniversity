sap.ui.define([
    "sap/ui/test/opaQunit",
    "./pages/ProductPage",
    "./pages/UserPage"
], function () {
    "use strict";

    var oConfig = {
        componentConfig: {
            name: "iba.gomel.by.myshop"
        }
    }

    QUnit.module("Navigation througth options");

    opaTest("Should navigate from primary to contact", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iNavigateToUserPage();
        When.onTheUserPage.iMoveToContactOption();

        Then.onTheUserPage.IamOnContactPageOption().and.iTeardownMyApp();
    });

    opaTest("Should navigate from primary to delivery", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iNavigateToUserPage();
        When.onTheUserPage.iMoveToDeliveryOption();

        Then.onTheUserPage.IamOnDeliveryPageOption().and.iTeardownMyApp();
    });

    opaTest("Should navigate from primary to private", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iNavigateToUserPage();
        When.onTheUserPage.iMoveToPrivateOption();

        Then.onTheUserPage.IamOnPrivatePageOption().and.iTeardownMyApp();
    });

    QUnit.module("Editing information");

    opaTest("Should activate edit mode", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iNavigateToUserPage();
        When.onTheUserPage.iPressEditButton();

        Then.onTheUserPage.editModeIsActive().and.iTeardownMyApp();
    });

    opaTest("Should deactivate edit mode after activating", function (Given, When, Then) {
        Given.iStartMyUIComponent(oConfig);
        When.onTheAppPage.iNavigateToUserPage();
        When.onTheUserPage.iPressEditButton().and.iPressCancelButton();

        Then.onTheUserPage.editModeIsNotActive().and.iTeardownMyApp();
    });

});