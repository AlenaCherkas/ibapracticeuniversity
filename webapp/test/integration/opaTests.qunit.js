QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
    "use strict";

    sap.ui.require([
        "iba/gomel/by/myshop/localService/mockserver",
        "iba/gomel/by/myshop/test/integration/ChosenNavigationJourney"
    ], function (mockserver) {
        mockserver.init();
        QUnit.start();
    });
 });