sap.ui.define(['sap/ui/core/ValueStateSupport', 'sap/ui/core/IndicationColorSupport', 'sap/ui/core/InvisibleText', 'sap/ui/core/library'],
   function (ValueStateSupport, IndicationColorSupport, InvisibleText, coreLibrary) {
      "use strict";

      var TextDirection = coreLibrary.TextDirection;
      var ValueState = coreLibrary.ValueState;

      var CurrencyRenderer = {
         apiVersion: 2
      };

      CurrencyRenderer.render = function (oRm, oObjStatus) {
         oRm.openStart("div", oObjStatus);

         if (oObjStatus._isEmpty()) {
            oRm.style("display", "none");
            oRm.openEnd();
         } else {

            var sState = oObjStatus.getState();
            var bInverted = oObjStatus.getInverted();
            var sTextDir = oObjStatus.getTextDirection();
            var oCore = sap.ui.getCore();
            var bPageRTL = oCore.getConfiguration().getRTL();
            var oAccAttributes = {
               roledescription: oCore.getLibraryResourceBundle("sap.m").getText("OBJECT_STATUS")
            };
            var sValueStateText;
            var accValueText;

            if (sTextDir === TextDirection.Inherit) {
               sTextDir = bPageRTL ? TextDirection.RTL : TextDirection.LTR;
            }

            var sTooltip = oObjStatus.getTooltip_AsString();
            if (sTooltip) {
               oRm.attr("title", sTooltip);
            }

            oRm.class("sapMObjStatus");
            oRm.class("sapMObjStatus" + sState);
            if (bInverted) {
               oRm.class("sapMObjStatusInverted");
            }

            if (oObjStatus._isActive()) {
               oRm.class("sapMObjStatusActive");
               oRm.attr("tabindex", "0");
               oAccAttributes.role = "button";
            } else {
               oAccAttributes.role = "group";
            }

            oRm.accessibilityState(oObjStatus, oAccAttributes);

            oRm.openEnd();

            if (oObjStatus.getTitle()) {

               oRm.openStart("span", oObjStatus.getId() + "-title");
               oRm.class("sapMObjStatusTitle");

               if (sTextDir) {
                  oRm.attr("dir", sTextDir.toLowerCase());
               }
               oRm.openEnd();
               oRm.text(oObjStatus.getTitle() + ":");
               oRm.close("span");
            }

            if (oObjStatus._isActive()) {
               oRm.openStart("span", oObjStatus.getId() + "-link");
               oRm.class("sapMObjStatusLink");
               oRm.openEnd();
            }

            if (oObjStatus.getIcon()) {
               oRm.openStart("span", oObjStatus.getId() + "-statusIcon");
               oRm.class("sapMObjStatusIcon");
               if (!oObjStatus.getText()) {
                  oRm.class("sapMObjStatusIconOnly");
               }
               oRm.openEnd();
               oRm.renderControl(oObjStatus._getImageControl());
               oRm.close("span");
            }

            if (oObjStatus.getCurr()) {
               var sCurrencyValue = oObjStatus.getCurr();
               oRm.openStart("span", oObjStatus.getId() + "-text");
               oRm.class("style" + sCurrencyValue);
               oRm.openEnd();
               oRm.text(sCurrencyValue);
               oRm.close("span");

            }

            if (oObjStatus._isActive()) {
               oRm.close("span");
            }
            if (sState != ValueState.None) {
               sValueStateText = ValueStateSupport.getAdditionalText(sState);
               if (sValueStateText) {
                  accValueText = sValueStateText;
               } else {
                  accValueText = IndicationColorSupport.getAdditionalText(sState);
               }
               if (accValueText) {
                  oRm.openStart("span", oObjStatus.getId() + "sapSRH");
                  oRm.class("sapUiPseudoInvisibleText");
                  oRm.openEnd();
                  oRm.text(accValueText);
                  oRm.close("span");
               }
            }

         }

         oRm.close("div");
      };

      return CurrencyRenderer;

   }, true);

