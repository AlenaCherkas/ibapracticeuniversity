sap.ui.define([
   'sap/ui/core/Control',
   'sap/ui/core/ValueStateSupport',
   'sap/ui/core/IndicationColorSupport',
   'sap/ui/core/library',
   'sap/ui/base/DataType',
   './CurrencyObjectStatusRenderer',
   "sap/m/ObjectStatus"
],
   function (Control, ValueStateSupport, IndicationColorSupport, coreLibrary, DataType, CurrencyObjectStatusRenderer, ObjectStatus) {
      "use strict";

      var TextDirection = coreLibrary.TextDirection;
      var ValueState = coreLibrary.ValueState;

      var CurrencyObjectStatus = ObjectStatus.extend("iba.gomel.by.myshop.control.CurrencyObjectStatus", {
         metadata: {
            interfaces: ["sap.ui.core.IFormContent"],
            library: "sap.m",
            designtime: "sap/m/designtime/ObjectStatus.designtime",
            properties: {
               title: { type: "string", group: "Misc", defaultValue: null },
               text: { type: "string", group: "Misc", defaultValue: null },
               curr: { type: "string", group: "Misc", defaultValue: null },
               active: { type: "boolean", group: "Misc", defaultValue: false },
               state: { type: "string", group: "Misc", defaultValue: ValueState.None },
               inverted: { type: "boolean", group: "Misc", defaultValue: false },
               icon: { type: "sap.ui.core.URI", group: "Misc", defaultValue: null },
               iconDensityAware: { type: "boolean", group: "Appearance", defaultValue: true },
               textDirection: { type: "sap.ui.core.TextDirection", group: "Appearance", defaultValue: TextDirection.Inherit }
            },
            associations: {
               ariaDescribedBy: { type: "sap.ui.core.Control", multiple: true, singularName: "ariaDescribedBy" }
            },
            events: {
               press: {}
            },
            dnd: { draggable: true, droppable: false }
         }
      });

      CurrencyObjectStatus.prototype.exit = function () {
         if (this._oImageControl) {
            this._oImageControl.destroy();
            this._oImageControl = null;
         }
      };

      CurrencyObjectStatus.prototype._getImageControl = function () {
         var sImgId = this.getId() + '-icon',
            bIsIconOnly = !this.getText() && !this.getTitle(),
            mProperties = {
               src: this.getIcon(),
               densityAware: this.getIconDensityAware(),
               useIconTooltip: false
            };

         if (bIsIconOnly) {
            mProperties.decorative = false;
            mProperties.alt = sap.ui.getCore().getLibraryResourceBundle("sap.m").getText("OBJECT_STATUS_ICON");
         }

         this._oImageControl = ImageHelper.getImageControl(sImgId, this._oImageControl, this, mProperties);

         return this._oImageControl;
      };

      CurrencyObjectStatus.prototype.setState = function (sValue) {
         if (sValue === null) {
            sValue = ValueState.None;
         } else if (!DataType.getType("sap.ui.core.ValueState").isValid(sValue) && !DataType.getType("sap.ui.core.IndicationColor").isValid(sValue)) {
            throw new Error('"' + sValue + '" is not a value of the enums sap.ui.core.ValueState or sap.ui.core.IndicationColor for property "state" of ' + this);
         }

         return this.setProperty("state", sValue);
      };

      CurrencyObjectStatus.prototype.setCurr = function (sValue) {
         return this.setProperty("curr", sValue);
      }

      CurrencyObjectStatus.prototype.ontap = function (oEvent) {
         if (this._isClickable(oEvent)) {
            this.firePress();
         }
      };


      CurrencyObjectStatus.prototype.onsapenter = function (oEvent) {
         if (this._isActive()) {
            this.firePress();
            oEvent.setMarked();
         }
      };


      CurrencyObjectStatus.prototype.onsapspace = function (oEvent) {
         this.onsapenter(oEvent);
      };

      CurrencyObjectStatus.prototype._isActive = function () {

         return this.getActive() && (this.getText().trim() || this.getIcon().trim());
      };

      CurrencyObjectStatus.prototype._isEmpty = function () {

         return !(this.getText().trim() || this.getIcon().trim() || this.getTitle().trim());
      };


      CurrencyObjectStatus.prototype.ontouchstart = function (oEvent) {
         if (this._isClickable(oEvent)) {
            oEvent.setMarked();
         }
      };


      CurrencyObjectStatus.prototype.getAccessibilityInfo = function () {
         var sState = ValueStateSupport.getAdditionalText(this.getState());

         if (this.getState() != ValueState.None) {
            sState = (sState !== null) ? sState : IndicationColorSupport.getAdditionalText(this.getState());
         }

         return {
            description: (
               (this.getTitle() || "") + " " +
               (this.getText() || "") + " " +
               (sState !== null ? sState : "") + " " +
               (this.getTooltip() || "") + " " +
               sap.ui.getCore().getLibraryResourceBundle("sap.m").getText("OBJECT_STATUS")
            ).trim()
         };
      };

      CurrencyObjectStatus.prototype._isClickable = function (oEvent) {
         var sSourceId = oEvent.target.id;

         return this._isActive() && (sSourceId === this.getId() + "-link" || sSourceId === this.getId() + "-text" || sSourceId === this.getId() + "-statusIcon" || sSourceId === this.getId() + "-icon");
      };

      return CurrencyObjectStatus;

   });