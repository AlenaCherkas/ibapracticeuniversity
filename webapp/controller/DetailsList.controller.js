sap.ui.define([
    "sap/ui/model/FilterOperator",
    "sap/ui/model/Filter",
    "sap/ui/core/Fragment",
    "sap/ui/model/Sorter",
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/f/LayoutType"
 ], function (FilterOperator, Filter, Fragment, Sorter, BaseController, JSONModel, LayoutType) {
    "use strict";

    return BaseController.extend("iba.gomel.by.myshop.controller.DetailsList", {

        onInit : function() {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("home").attachPatternMatched(this._onObjectMatched, this);
        },

        _onObjectMatched: function(oEvent) {
            this.setUserModel("Currency", "personOdata", false);
            this.getModelByName("oDataModel").refresh();
        },
           
        onFilter : function(oEvent) {
            var sQuery = oEvent.getParameter("query");
            var oFilter = new Filter({
                filters: [
                    new Filter({
                        path: "ProductName",
                        operator: FilterOperator.Contains,
                        value1: sQuery
                    }),
                    new Filter({
                        path: 'Type',
                        operator: FilterOperator.Contains,
                        value1: sQuery
                    }),					
                    new Filter({
                        path: 'Year',
                        operator: FilterOperator.Contains,
                        value1: sQuery
                    })
                ],
                and: false
            });

            var oList = this.byId("detailsList");
            var oBinding = oList.getBinding("items");
            oBinding.filter(oFilter);
        },

        onCustomFilter : function(oEvent) {
            var oView = this.getView();
            var oGroupDialog = this.byId("groupDialog");
            if (!oGroupDialog) {
                Fragment.load({
                    id: oView.getId(),
                    name: "iba.gomel.by.myshop.fragment.GroupSettingDialog",
                    controller: this
                }).then(function(oDialog){
                    oView.addDependent(oDialog);
                    oDialog.open();
                }.bind(this));
            } else {
                oGroupDialog.open();
            }
        },

        onCustomSort : function(oEvent) {
            var oView = this.getView();
            var oGroupDialog = this.byId("sortDialog");
            if (!oGroupDialog) {
                Fragment.load({
                    id: oView.getId(),
                    name: "iba.gomel.by.myshop.fragment.SortSettingDialog",
                    controller: this
                }).then(function(oDialog){
                    oView.addDependent(oDialog);
                    oDialog.open();
                }.bind(this));
            } else {
                oGroupDialog.open();
            }
        },

        onCustomFiltering : function(oEvent) {
            var oView = this.getView();
            if (oView.getModel() === undefined) {
                var oDataModel = this.getModelByName("oDataModel");
                var iMax = oDataModel.oData[this.byId("detailsList").getItems()[0].getBindingContext("oDataModel").getPath().substr(1)].Year;
                var iMin = iMax;
                var aItems = this.byId("detailsList").getItems();
                var aTypes = [];
                var sTypeNames = []
                var iLength = aItems.length;
                for (var i = 0; i < iLength; i++) {
                    var oItem = aItems[i];
                    var oDetail = oDataModel.oData[oItem.getBindingContext("oDataModel").getPath().substr(1)]
                    var iYear = oDetail.Year;
                    if (iYear > iMax) {
                        iMax = iYear;
                    }
                    if (iYear < iMin) {
                        iMin = iYear;
                    }
                    var sType = oDetail.Type;

                    if (sTypeNames.indexOf(sType) == -1) {
                        var oType = {
                            "text": sType,
                            "key": "EQ__Type__" + sType
                        };
                        aTypes.push(oType);
                        sTypeNames.push(sType);  
                    }
                    
                }

                var oModel = new JSONModel();
                oModel.setData(aTypes);
                this.setModel(oModel, "types");

                var aRange = [
                    parseInt(iMin),
                    parseInt(iMax)                  
                ];

                var oData = {
                    rangeinfo : {
                        "max" : aRange[1],
                        "min" : aRange[0],
                        "range" : aRange
                    }
                }

                var oModel = new JSONModel(oData);
                oView.setModel(oModel);
            } 

            var oGroupDialog = this.byId("filterDialog");
            if (!oGroupDialog) {
                Fragment.load({
                    id: oView.getId(),
                    name: "iba.gomel.by.myshop.fragment.FilterSettingDialog",
                    controller: this
                }).then(function(oDialog){
                    oView.addDependent(oDialog);
                    oDialog.open();
                }.bind(this));
            } else {
                oGroupDialog.open();
            }
        },

        onConfirmGroping : function(oEvent) {	
            var aParams = oEvent.getParameters();
            var sPath;
            var bDescending;
            var aSorters = [];

            if (aParams.groupItem) {
                sPath = aParams.groupItem.getKey();
                bDescending = aParams.groupDescending;
                var bGroup = true;
                var oSorter = new Sorter(sPath, bDescending, bGroup);
                aSorters.push(oSorter);
            }

            this.byId("detailsList").getBinding("items").sort(aSorters);
        },

        onConfirmSorting: function(oEvent) {
            var aParams = oEvent.getParameters();
            var sPath = aParams.sortItem.getKey();
            var bDescending = aParams.sortDescending;
            var aSorters = [];

            aSorters.push(new Sorter(sPath, bDescending));
            this.byId("detailsList").getBinding("items").sort(aSorters);
        },

        onConfirmFiltering: function(oEvent) {
            var aParams = oEvent.getParameters();
            var aFilter = [];

            if (aParams.filterItems.length > 0) {
                aParams.filterItems.forEach(function(oItem) {
                    var aSplit = oItem.getKey().split("__");
                    if (aSplit[0] === "EQ") {
                        aFilter.push(new Filter(aSplit[1], FilterOperator.EQ, aSplit[2]));
                    } 
                });
            } else {
                var oRangeSlider = this.byId("rangeYear");
                var iValue1 = oRangeSlider.getValue();
                var iValue2 = oRangeSlider.getValue2();
                aFilter.push(new Filter("Year", FilterOperator.BT, iValue1, iValue2));
            }

            var oList = this.byId("detailsList");
            var oBinding = oList.getBinding("items");
            oBinding.filter(aFilter);
        },

        rangeChange: function(oEvent) {
            var oRangeSlider = oEvent.getSource();
            var iValue1 = oRangeSlider.getValue();
            var iValue2 = oRangeSlider.getValue2();
            if (iValue1 > iValue2) {
                oRangeSlider.setValue(iValue2);
                oRangeSlider.setValue2(iValue1);                  
            }
        },

        onListItemPress: function(oEvent) {
            this.getOwnerComponent().getRootControl().byId("fcl").setLayout(LayoutType.TwoColumnsBeginExpanded);
            this.oRouter.navTo("detail", {
                detail: window.encodeURIComponent(oEvent.getSource().getBindingContext("oDataModel").getPath().substr(1))
            });
        },
    });
 });