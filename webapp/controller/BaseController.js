sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageToast",
    "sap/m/Dialog",
    "sap/m/Text",
    "sap/m/Button",
    "sap/m/ButtonType"
], function (Controller, JSONModel, MessageToast, Dialog, Text, Button, ButtonType) {
    "use strict";

    return Controller.extend("iba.gomel.by.myshop.controller.BaseController", {

        getODataModel: function () {
            return this.getView().getModel("oDataModel");
        },

        getErrorMessage: function(sErrorText) {
            var sResultTextError = "";
            var oController = this;
            if (sErrorText === undefined) {
                sResultTextError = oController.getBundle().getText("errorUndefinedErrorText")
            } else {
                sResultTextError = sErrorText;
            }
            return oController.getBundle().getText("errorText", [sResultTextError]);
        },

        getModelByName: function(sName) {
            return this.getView().getModel(sName);
        },

        setModel: function(oModel, sName) {
            return this.getView().setModel(oModel, sName);
        },

        getModelProperty: function(sName, sProperty) {
            return this.getView().getModel(sName).getProperty(sProperty);
        },

        setModelProperty: function(sName, sProperty, oData) {
            return this.getView().getModel(sName).setProperty(sProperty, oData);
        },

        setUserModel: function(sExpandParameters, sModelName, bIsDeepInto) {
            var oDataModel = this.getODataModel();
            var oController = this;
            oDataModel.read("/Users", {
                urlParameters: {
                    $expand: sExpandParameters,
                    $top: 1
                },
                success: function (oData, oResponse) {
                    var oModel = new JSONModel();
                    if (bIsDeepInto) {
                        oModel.setData(oData.results[0].Card.CardDetails.results);
                    } else if (bIsDeepInto == false) {
                        oModel.setData(oData.results[0]);
                    } else {
                        oModel.setData(oData.results[0].Card.ChosenDetails.results);
                    }
                    oController.setModel(oModel, sModelName);
                },
                error: function (oErr) {
                    oController.showErrorPopupMessage(oErr.message);
                }
            });
        },

        setCurrencies: function() {
            var oDataModel = this.getODataModel();
            var oController = this;
            oDataModel.read("/Currencies", {
                success: function (oData, oResponse) {
                    var oModel = new JSONModel();
                    oModel.setData(oData.results);
                    oController.setModel(oModel, "currencies");
                },
                error: function (oErr) {
                    oController.showErrorPopupMessage(oErr.message);
                }
            });
        },

        getBundle: function() {
            return this.getView().getModel("i18n").getResourceBundle();
        },

        copyUser: function(oUser) {
            var oNewUser = {};
            oNewUser.ID = oUser.ID;
            oNewUser.CardID = oUser.CardID
            oNewUser.CurrencyID = oUser.CurrencyID;
            oNewUser.firstName = oUser.firstName;
            oNewUser.lastName = oUser.lastName;
            oNewUser.userName = oUser.userName;
            oNewUser.phone = oUser.phone;
            oNewUser.email = oUser.email;
            oNewUser.adress = oUser.adress;
            oNewUser.city = oUser.city;
            oNewUser.balance = oUser.balance;
            oNewUser.postIndex = oUser.postIndex;

            return oNewUser;
        },

        showErrorPopupMessage: function(sMessage) {
            var oBundle = this.getBundle();
            var oDialog = new Dialog({
                title: oBundle.getText("errorPopupTitle"),
                type: 'Message',
                state: 'Error',
                content: new Text({
                    text: sMessage
                }),
                beginButton: new Button({
                    type: ButtonType.Emphasized,
                    text: oBundle.getText("errorButtonPopupText"),
                    press: function () {
                        oDialog.close();
                    }
                }),
                afterClose: function () {
                    oDialog.destroy();
                }
            });

            oDialog.open();
        }
    });
});