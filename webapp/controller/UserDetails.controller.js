sap.ui.define([
    "./BaseController",
    "sap/ui/model/SimpleType",
    "sap/ui/model/ValidateException",
    "sap/ui/model/resource/ResourceModel"
], function (BaseController, SimpleType, ValidateException, ResourceModel) {
    "use strict";

    return BaseController.extend("iba.gomel.by.myshop.controller.UserDetails", {

        onInit : function() {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("userinfo").attachPatternMatched(this._onObjectMatched, this);

            this._inputFirstName = this.byId("inputFirstName");
            this._inputLastName = this.byId("inputLastName");
            this._inputUserName = this.byId("inputUserName");

            this._inputEmail = this.byId("inputEmail");
            this._inputPhone = this.byId("inputPhone");

            this._inputCity = this.byId("inputCity");
            this._inputAdress = this.byId("inputAdress");
            this._inputPostIndex = this.byId("inputPostIndex");

            this._buttonAccept = this.byId("accept");
            this._buttonReject = this.byId("reject");
            this._buttonEdit = this.byId("edit");

            this._comboboxCurrency = this.byId("currencyCB");

            this.oBundle = new ResourceModel({
                bundleName: "iba.gomel.by.myshop.i18n.i18n"
            }).getResourceBundle();
        },

        _setUserModel: function(oEvent) {
            this.setUserModel("Currency", "personOdata", false);
        },

        _onObjectMatched: function(oEvent) {
            this.onChangeEditable(false);
            this._buttonEdit.setVisible(true);
            this._setUserModel();
            this.setCurrencies();
        },

        onPressPrimaryOption: function(oEvent) {
            this.getSplitAppObj().to(this.createId("primaryPage"));		
        },

        onPressContactOption: function(oEvent) {
            this.getSplitAppObj().to(this.createId("contactPage"));		
        },

        onPressDeliveryOption: function(oEvent) {
            this.getSplitAppObj().to(this.createId("deliveryPage"));		
        },

        onPressPrivateOption: function(oEvent) {
            this.getSplitAppObj().to(this.createId("privatePage"));	
            this._setUserModel();
            this.byId("currencyCB").setSelectedKey(this.getModelByName("personOdata").getData().Currency.ID);
        },

        getSplitAppObj: function() {
            var result = this.byId("SplitAppDemo");
            return result;
        },

        onReject: function(oEvent) {
            this._setUserModel();
            this.byId("currencyCB").setSelectedKey(this.getModelByName("personOdata").getData().Currency.ID);
            this.onChangeEditable(!oEvent.getSource().getVisible());
        },

        onAccept: function(oEvent) {
            if (!this.validate()) {
                var oController = this;
                this.onChangeEditable(false);
                var oUserModel = this.getModelByName("personOdata").getData();
   
                var oUser = {};
                oUser.ID = oUserModel.ID;
                oUser.CardID = oUserModel.CardID
                oUser.CurrencyID = parseInt(this._comboboxCurrency.getSelectedKey());
                oUser.firstName = this._inputFirstName.getValue();
                oUser.lastName = this._inputLastName.getValue();
                oUser.userName = this._inputUserName.getValue();
                oUser.phone = this._inputPhone.getValue();
                oUser.email = this._inputEmail.getValue();
                oUser.adress = this._inputAdress.getValue();
                oUser.city = this._inputCity.getValue();
                oUser.balance = oUserModel.balance;
                oUser.postIndex = this._inputPostIndex.getValue();
                     
                var oModel = this.getODataModel();
                var oController = this;
                oModel.update("/Users(" + oUser.ID +")", oUser, {
                    method: "PUT",
                    success: function() {
                        oController._buttonAccept.setVisible(false);
                        oController._buttonReject.setVisible(false);
                        oController._buttonEdit.setVisible(true);
                        oController._onObjectMatched()
                    },
                    error: function(oErr) {
                        oController.showErrorPopupMessage(oErr.message);
                    }
                })
            }
        },

        onEdit: function(oEvent) {
            var bState = oEvent.getSource().getVisible();
            this.onChangeEditable(bState);
        },

        onChangeEditable: function(bState) {
            this._inputFirstName.setEditable(bState);
            this._inputLastName.setEditable(bState);
            this._inputUserName.setEditable(bState);

            this._inputEmail.setEditable(bState);
            this._inputPhone.setEditable(bState);

            this._inputCity.setEditable(bState);
            this._inputAdress.setEditable(bState);
            this._inputPostIndex.setEditable(bState);

            this._buttonAccept.setVisible(bState);
            this._buttonReject.setVisible(bState);
            this._buttonEdit.setVisible(!bState);

            this._comboboxCurrency.setEditable(bState);
        },

        _validateInput: function(oInput) {
            var oBinding = oInput.getBinding("value");
            var sValueState = "None";
            var bIsValid = false;

            try {
                oBinding.getType().validateValue(oInput.getValue());
            } catch (oExeption) {
                sValueState = "Error";
                bIsValid = true;
            }

            oInput.setValueState(sValueState);
            return bIsValid;
        },

        _validateMaskInput: function(oMaskInput) {
            var bIsValid = false;
            var sValueState = "None";
            var sValue = oMaskInput.getValue();
            if (sValue.indexOf("_") != -1) {
                sValueState = "Error";
                bIsValid = true;                
            }
            oMaskInput.setValueState(sValueState);
            return bIsValid;
        },

        validate: function() {
            var oView = this.getView();
            var aInputs = [
                oView.byId("inputEmail")
            ];
            var aMaskInputs = [
                oView.byId("inputPhone")
            ];
            var bIsNotValid = false;

            var iLength = aInputs.length;
            for (var i = 0; i < iLength; i++) {
                var oInput = aInputs[i];
                bIsNotValid = bIsNotValid || this._validateInput(oInput);
            }

            iLength = aMaskInputs.length;
            for (var i = 0; i < iLength; i++) {
                var oMaskInput = aMaskInputs[i];
                bIsNotValid = bIsNotValid || this._validateMaskInput(oMaskInput);
            }

            if (bIsNotValid) {
                this.showErrorPopupMessage(this.oBundle.getText("validateErrorText"));                
            }

            return bIsNotValid;
        },

        emailType: SimpleType.extend("email", {
            formatValue: function(oValue) {
                return oValue;
            },
            parseValue: function(oValue) {
                return oValue;
            },
            validateValue: function (oValue) {
                var rexMail = /^\w+[\w-+\.]*\@\w+([-\.]\w+)*\.[a-zA-Z]{2,}$/;
                if (!oValue.match(rexMail)) {
                    throw new ValidateException();
                }
            }
        })
    });
});