sap.ui.define([
    "../formatter/totalPriceFormatter",
    "sap/m/MessageToast",
    "./BaseController",
    "sap/ui/model/resource/ResourceModel",
    "sap/m/MessageBox",
    "sap/ui/core/Fragment",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/BindingMode"
], function (totalPriceFormatter, MessageToast, BaseController, ResourceModel, MessageBox, Fragment, JSONModel, BindingMode) {
    "use strict";

    return BaseController.extend("iba.gomel.by.myshop.controller.Card", {
        formatter: totalPriceFormatter,
        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("card").attachPatternMatched(this._onObjectMatched, this);
            this.oBundle = new ResourceModel({
                bundleName: "iba.gomel.by.myshop.i18n.i18n"
            }).getResourceBundle();
        },

        _onObjectMatched: function(oEvent) {
            this.setUserModel("Currency,Card,Card/CardDetails,Card/CardDetails/Card,Card/CardDetails/Detail,Card/CardDetails/Detail/StockInfo", "goods", true);
            this.setUserModel("Currency", "personOdata", false);
        },

        onCountChange: function(oEvent) {
            var oStepInput = oEvent.getSource();
            if (oStepInput.getValueState() != "None") {
                oStepInput.setValue(oStepInput.getMin());
                this.showErrorPopupMessage(this.oBundle.getText("stepInputOurOfRange"));
                oStepInput.setValueState("None");
            } else {
                var oController = this;
                var oCardDetail = this.getModelByName("goods").getData()[oEvent.getSource().getBindingContext("goods").getPath().slice(1)];
                var oDataModel = this.getODataModel();
                oDataModel.update("/CardDetails(" + oCardDetail.ID + ")", oCardDetail, {
                    method: "PUT",
                    error: function (oErr) {
                        oController.showErrorPopupMessage(oErr.message);
                    }
                })               
            }
            this._onObjectMatched();
        },

        removeAll: function(oEvent) {
            var oController = this;
            var oCardDetail = this.getModelByName("goods").getData()[oEvent.getSource().getBindingContext("goods").getPath().slice(1)];
            var oDataModel = this.getODataModel();
            oDataModel.remove("/CardDetails(" + oCardDetail.ID + ")", {
                success: function () {
                    MessageToast.show(oController.oBundle.getText("productsHasBeenRemoved"));
                },
                error: function (oErr) {
                    oController.showErrorPopupMessage(oErr.message);
                }
            });
            this._onObjectMatched();
        },

        cleanUp: function(oEvent) {
            var oController = this;
            var aGoods = this.getModelByName("goods").getData();
            if (aGoods.length) {
                var oDataModel = this.getODataModel();
                oDataModel.setDefaultBindingMode(BindingMode.TwoWay);
                oDataModel.setUseBatch(true);
                oDataModel.setDeferredGroups(["cleanUP"]);

                var iLength = aGoods.length;
                for (var i = 0; i < iLength; i++) {
                    oDataModel.remove("/CardDetails(" + aGoods[i].ID + ")", {});
                }

                oDataModel.submitChanges({
                    groupid: "cleanUP",
                    success: function () {
                        MessageToast.show(oController.oBundle.getText("successCleanUp"));
                    },
                    error: function (oErr) {
                        oController.showErrorPopupMessage(oErr.message);
                    }
                });
            }
            this._onObjectMatched();
        },

        checkOut: function(oEvent) {
            MessageBox.confirm(this.oBundle.getText("cardCheckoutQuestion"), { 
                onClose: function(sAction) {
                    switch (sAction) {
                        case MessageBox.Action.OK:
                        var oController = this;
                        var aGoods = this.getModelByName("goods").getData();
                        var oUser = this.getModelByName("personOdata").getData();
                        if ((oUser.balance * oUser.Currency.Ratio) >= this.getTotalSumm(aGoods, oUser.Currency)) {
                            var oDataModel = this.getODataModel();
                            oDataModel.setDefaultBindingMode(BindingMode.TwoWay);
                            oDataModel.setUseBatch(true);
                            oDataModel.setDeferredGroups(["checkout"]);

                            var iSumm = 0;
                            var iLength = aGoods.length;
                            for (var i = 0; i < iLength; i++) {
                                if (aGoods[i].IsBuyNow) {
                                    iSumm += aGoods[i].Detail.Price * aGoods[i].Count;
                                    var oStockModel = {};
                                    oStockModel.ID = aGoods[i].Detail.StockInfo.ID;
                                    oStockModel.Count = aGoods[i].Detail.StockInfo.Count - aGoods[i].Count;
                                    oDataModel.update("/StockInfos(" + oStockModel.ID + ")", oStockModel, {method: "PUT"});
                                    oDataModel.remove("/CardDetails(" + aGoods[i].ID + ")", {});
                                }
                            }
                            var oUserRecount = this.copyUser(oUser);
                            oUserRecount.balance = oUserRecount.balance - iSumm;
                            oDataModel.update("/Users(" + oUserRecount.ID + ")", oUserRecount, {method: "PUT"});

                            oDataModel.submitChanges({
                                groupid: "checkout",
                                success: function () {
                                    MessageToast.show(oController.oBundle.getText("successCheckout"));
                                },
                                error: function (oErr) {
                                    oController.showErrorPopupMessage(oErr.message);
                                }
                            });
                        } else {
                            MessageToast.show(this.oBundle.getText("notEnoughMoney"));
                        }
                        this._onObjectMatched();                            
                            break;
                        case MessageBox.Action.CANCEL:
                            break;
                    }
                }.bind(this)
            });
        },

        showMoreInfo: function(oEvent) {
            var oView = this.getView();
            var oButton = oEvent.getSource();
            var oCardDetail = this.getModelByName("goods").getData()[oEvent.getSource().getBindingContext("goods").getPath().slice(1)];

            var oData = this.prepareModel(oCardDetail)
            var oModel = new JSONModel(oData);
            this.getView().setModel(oModel, "pages");

            if (!this._oQuickView) {
                Fragment.load({
                    name: "iba.gomel.by.myshop.fragment.QuickView",
                    controller: this
                }).then(function (oQuickView) {
                    this._oQuickView = oQuickView;
                    oView.addDependent(this._oQuickView);
                    this._oQuickView.openBy(oButton);
                }.bind(this));
            } else {
                oView.addDependent(this._oQuickView);
                this._oQuickView.openBy(oButton);
            }
        },

        onSelect: function (oEvent) {
            var oController = this;
            var oCardDetail = this.getModelByName("goods").getData()[oEvent.getSource().getBindingContext("goods").getPath().slice(1)];
            var oDataModel = this.getODataModel();
            oDataModel.update("/CardDetails(" + oCardDetail.ID + ")", oCardDetail, {
                method: "PUT",
                error: function (oErr) {
                    oController.showErrorPopupMessage(oErr.message);
                }
            })

            this._onObjectMatched();
        },

        getTotalSumm: function (aGoods, oCurrency) {
            var iSumm = 0;
            var iLength = aGoods.length;
            if (iLength > 0) {
                for (var i = 0; i < iLength; i++) {
                    iSumm += aGoods[i].Count * aGoods[i].Detail.Price;
                }
            }
            return iSumm * oCurrency.Ratio;
        },

        prepareModel: function (oCardDetail) {
            var aModels = [
                {
                    "productName": oCardDetail.Detail.ProductName,
                    "image": oCardDetail.Detail.ImageSrc,
                    "type": oCardDetail.Detail.Type,
                    "description": oCardDetail.Detail.Description,
                    "country": oCardDetail.Detail.Country,
                    "manufacturer": oCardDetail.Detail.Manufacturer,
                    "seller": oCardDetail.Detail.SellerName,
                    "email": oCardDetail.Detail.SellerEmail,
                    "phone": oCardDetail.Detail.SellerPhone,
                    "quantity": oCardDetail.Detail.StockInfo.Count                    
                }
            ];

            return aModels;
        }
    });
});