sap.ui.define([
    "./BaseController",
    "sap/ui/model/resource/ResourceModel",
    "sap/m/MessageToast",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator"
], function (BaseController, ResourceModel, MessageToast, Filter, FilterOperator) {
    "use strict";

    return BaseController.extend("iba.gomel.by.myshop.controller.Chosen", {
        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("chosen").attachPatternMatched(this._onObjectMatched, this);
            this.oBundle = new ResourceModel({
                bundleName: "iba.gomel.by.myshop.i18n.i18n"
            }).getResourceBundle();
        },

        _onObjectMatched: function (oEvent) {
            this.setUserModel("Currency,Card,Card/ChosenDetails,Card/ChosenDetails/Card,Card/ChosenDetails/Detail,Card/ChosenDetails/Detail/StockInfo", "chosens", undefined);
            this.setUserModel("Currency", "personOdata", false);
        },

        removeChosen: function (oEvent) {
            var oController = this;
            var oChosenDetail = this.getModelByName("chosens").getData()[oEvent.getSource().getBindingContext("chosens").getPath().slice(1)];
            var oDataModel = this.getODataModel();
            oDataModel.remove("/ChosenDetails(" + oChosenDetail.ID + ")", {
                success: function () {
                    MessageToast.show(oController.oBundle.getText("chosenProductHasBeenRemoved"));
                },
                error: function (oErr) {
                    oController.showErrorPopupMessage(oErr.message);
                }
            });
            this._onObjectMatched();
        },

        addToCard: function (oEvent) {
            var oController = this;
            var oChosenDetail = this.getModelByName("chosens").getData()[oEvent.getSource().getBindingContext("chosens").getPath().slice(1)];
            var oDataModel = this.getODataModel();

            oDataModel.read("/CardDetails", {
                filters: [
                    new Filter({
                        path: "DetailID",
                        operator: FilterOperator.EQ,
                        value1: oChosenDetail.DetailID
                    }),
                    new Filter({
                        path: "CardID",
                        operator: FilterOperator.EQ,
                        value1: oController.getModelProperty("personOdata", "/CardID")
                    })
                ],
                success: function (oData, oResponse) {
                    if (oData.results.length === 0) {
                        var oNewCardDetail = {
                            "CardID": oController.getModelProperty("personOdata", "/CardID"),
                            "DetailID": oChosenDetail.DetailID,
                            "Count": 1,
                            "IsBuyNow": true
                        }
                        oDataModel.create("/CardDetails", oNewCardDetail, {
                            method: "POST",
                            success: function (data) {
                                MessageToast.show(oController.oBundle.getText("productHasBeenAdded"));
                            },
                            error: function (oErr) {
                                oController.showErrorPopupMessage(oErr.message);
                            }
                        });
                    } else {
                            MessageToast.show(oController.oBundle.getText("productHasBeenAddedBefore"));
                        }
                    },
                    error: function (oErr) {
                        oController.showErrorPopupMessage(oErr.message);
                    }
                });

            this._onObjectMatched();
        },
    });
});