sap.ui.define([], function() {
    "use strict";
    return {
        totalPrice: function(aDetailsList, oCurrency) {
            var iSumm = 0;
            var iLength = aDetailsList.length;
            if (iLength > 0) {
                for (var i = 0; i < iLength; i++) {
                    if (aDetailsList[i].IsBuyNow) {
                        iSumm += aDetailsList[i].Count * aDetailsList[i].Detail.Price;
                    }
                }
                return iSumm * oCurrency.Ratio;
            } else {
                return 0;
            }
        }
    }
});