sap.ui.define([
    "sap/ui/core/ComponentContainer"
], function (ComponentContainer) {
    "use strict";

    new ComponentContainer({
        name: "iba.gomel.by.myshop",
        settings : {
            id : "myshop"
        },
        async: true
    }).placeAt("content");
});