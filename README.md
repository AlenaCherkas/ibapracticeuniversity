# Индивидуальный проект

Интернет-магазин по продаже компьютерных комплектующих.

## Требования:
  - Наличие меню сверху. Оно должно быть на всех страницах;
  - Навагиция по приложению;
  - Отображение баланса пользователя;
  - Кнопка изменения валюты;
  - С помощью ротеров и таргетов передвижение по 3-м страницам (Список товаров, Корзина, Пользовательская информация);
  - В пользовательском информации отобразить информацию о пользователе: ФИО, электронная почта, адрес, выбранная валюта;
  - Использование i18n;
  - По нажатию на товар сбоку должно развернутся окошко с подробной информацией о выбранном товаре(как на примере);
  - Там должно быть только две кнопки - Close и Full screan;
  - В окне подробной информации должно быть несколько вкладок - инфо о магазине продавце, инфо о доставке и т.д;
  - Где-нибудь разместить футер. Возможность помещать в корзину выбранный товар. А при попытке добавить в корзину уже имеющийся там товар уведомить пользователя. У пользователя есть только одна корзина. Один и тот же товар может лежать в ней в количестве более 1 штуки. Количество товаров на складе хранится в отдельной таблице, связанной с таблицей продуктов связью 1 к 1.
  - Создать кастомный компонент price для отображения валют в разных цветах: евро - розовый, доллар - салатовый, рубль белорусский - голубой.